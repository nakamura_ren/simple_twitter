<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>つぶやき編集画面</title>
</head>
<body>

	<div class="header">
		<a href="./">ホーム</a> <a href="setting">設定</a> <a href="logout">ログアウト</a>
	</div>


	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
			</ul>
		</div>
	</c:if>



	<div class="form-area">
		<form action="editMessages" method="post">
			<textarea name="text" cols="100" rows="5" class="tweet-box"><c:out
					value="${message.text}" /></textarea>
			<br /> <input type="submit" value="更新">（140文字まで） <input
				type="hidden" name="id" value="${message.id}" /> <a href="./">戻る</a>
		</form>
	</div>
</body>
</html>