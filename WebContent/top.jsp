<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>簡易Twitter</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>

			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="setting">設定</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>
		<!--ユーザー情報表示  -->
		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
				<div class="account">
					@
					<c:out value="${loginUser.account}" />
				</div>
				<div class="description">
					<c:out value="${loginUser.description}" />
				</div>
			</div>
		</c:if>

		<!-- エラー文-->
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="./" method="get">
			日付：<input type="date" name="start" value="${startDate}">～ <input
				type="date" name="end" value="${endDate}"> <input
				type="submit" value="絞込">
		</form>

		<div class="form-area">
			<c:if test="${ isShowMessageForm }">
				<form action="message" method="post">
					いま、どうしてる？<br />
					<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
					<br /> <input type="submit" value="つぶやく">（140文字まで）
				</form>
			</c:if>
		</div>

	</div>

	<div class="messages">
		<c:forEach items="${messages}" var="message">
			<div class="message">
				<div class="account-name">
					<span class="account"> <a
						href="./?user_id=<c:out value="${message.userId}"/> "> <c:out
								value="${message.account}" />
					</a>
					</span> <span class="name"><c:out value="${message.name}" /></span>
				</div>
				<div class="text">
					<pre><c:out value="${message.text}" /></pre>
				</div>
				<div class="date">
					<fmt:formatDate value="${message.createdDate}"
						pattern="yyyy/MM/dd HH:mm:ss" />
				</div>

				<c:if
					test="${ not empty loginUser && loginUser.id == message.userId}">
					<form action="deleteMessages" method="post">
						<input type="hidden" name="id" value="${message.id} }" /> <input
							type="submit" value="削除">
					</form>
				</c:if>

				<c:if
					test="${ not empty loginUser && loginUser.id == message.userId}">
					<form action="editMessages" method="get">
						<input type="hidden" name="id" value="${message.id}" /> <input
							type="submit" value="編集">
					</form>
				</c:if>
			</div>

			<!-- コメントの内容を表示 -->
			<c:forEach items="${comments}" var="comment">
				<c:if test="${comment.messageId == message.id }">
					<div class="account-name">
						<span class="account"> <c:out value="${comment.account }" />
						</span> <span class="name"><c:out value="${comment.name }" /></span>
					</div>

					<div class="text">
						<pre><c:out value="${comment.text}" /></pre>
					</div>

					<div class="date">
						<fmt:formatDate value="${comment.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
				</c:if>
			</c:forEach>

			<!--返信機能追加 -->
			<div class="form-area">
				<c:if test="${isShowMessageForm }">
					<form action="comment" method="post">
						<br /> <input type="hidden" name="id" value="${message.id }" />
						返信<br />
						<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
						<br /> <input type="submit" value="返信">（140文字まで）
					</form>
				</c:if>
			</div>

		</c:forEach>


		<div class="copyright">Copyright(c)nakamura ren</div>
	</div>
</body>
</html>