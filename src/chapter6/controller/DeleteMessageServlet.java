package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.service.MessageService;

/**
 * Servlet implementation class DeleteMessageServlet
 */
@WebServlet(urlPatterns = "/deleteMessages")
public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//jspからのnameを受け取る。valueのidを受け取る
		String id = request.getParameter("id");
		new MessageService().delete(id);
		response.sendRedirect("./");
	}

}
